package fr.azerks.stockmanager.controller.object;

public class Item {

    private long ID;
    private String name;
    private int quantity;
    private Type type;

    public enum Type{
        FRUITS, VEGETABLES, ALCOOL, DRINK, FOOD, DEFAULT;
    }

    public Item(){

    }

    public Item(long ID, String name, int quantity, Type type) {
        this.ID = ID;
        this.name = name;
        this.quantity = quantity;
        this.type = type;
    }

    @Override
    public String toString() {
        return "Item [ "
                + "id='" + ID + '\''
                + ", name='" + name + '\''
                + ", quantity='" + quantity + '\''
                + ", type='"  + type + '\''
                + ']';
    }

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setType(Type type){
        this.type = type;
    }

    public Type getType() {
        return type;
    }
}
