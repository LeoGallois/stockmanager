package fr.azerks.stockmanager.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import fr.azerks.stockmanager.controller.object.Item;

public class GsonController {
    Gson gson = new Gson();
    private List<Item> list = new ArrayList<>();

    public void Deserialize() {
        Type listType = new TypeToken<ArrayList<Item>>() {
        }.getType();
        String jsonItem = ""; // JSON file
        ArrayList<Item> items = gson.fromJson(jsonItem, listType);
    }

    public void Serialize() {
        Type listType = new TypeToken<ArrayList<Item>>() {
        }.getType();
        String result = gson.toJson(list, listType);
    }

    public String jsonParser(String json) {
        JsonParser parser = new JsonParser();
        JsonObject jsonObject = parser.parse(json).getAsJsonObject();

        Gson gson = new GsonBuilder()
                .serializeNulls()
                .disableHtmlEscaping()
                .setPrettyPrinting()
                .create();
        return gson.toJson(jsonObject);
    }
}
