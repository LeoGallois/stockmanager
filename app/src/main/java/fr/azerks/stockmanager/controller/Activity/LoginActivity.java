package fr.azerks.stockmanager.controller.Activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import fr.azerks.stockmanager.R;

public class LoginActivity extends AppCompatActivity {

   private EditText mEmail_Edit;
   private EditText mPassword_Edit;
   private Button mLogin_btn;
   private CheckBox mCheckBoxLogin;
   public static final String FAV_EMAIL= "fav_email";
   public static final String FAV_PASSWORD = "fav_password";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mEmail_Edit = findViewById(R.id.m_email_edit);
        mPassword_Edit = findViewById(R.id.m_password_edit);
        mLogin_btn = findViewById(R.id.m_login_btn);
        mCheckBoxLogin = findViewById(R.id.m_login_save_checkbox);

        mLogin_btn.setEnabled(false);
        mCheckBoxLogin.setChecked(true);

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        final SharedPreferences.Editor editor = pref.edit();

        if(pref.contains(FAV_EMAIL) && pref.contains(FAV_PASSWORD)) {
            mEmail_Edit.setText(pref.getString(FAV_EMAIL, null));
            mPassword_Edit.setText(pref.getString(FAV_PASSWORD, null));
            mLogin_btn.setEnabled(true);
        }
        else {
            mEmail_Edit.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    mLogin_btn.setEnabled(s.toString().length() >= 5);
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });
        }

        mLogin_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = mEmail_Edit.getText().toString();
                String password = mPassword_Edit.getText().toString();

                //String usernameBDD = sUser.getUsername();
                //String emailBDD = sUser.getEmail();
                //String passBDD = sUser.getPassword();

                if (email.equals("admin") && password.equals("admin")){
                    if (mCheckBoxLogin.isChecked()){
                        editor.putString(FAV_EMAIL, email);
                        editor.putString(FAV_PASSWORD, password);
                        editor.commit();
                    }
                    Intent stockActivityIntent = new Intent(LoginActivity.this, StockActivity.class);
                    startActivity(stockActivityIntent);
                }
                else {
                    Toast t = new Toast(LoginActivity.this).makeText(LoginActivity.this, "Failed to connect wrong Username/Password", Toast.LENGTH_SHORT);;
                    t.show();
                }
            }
        });
    }
}
