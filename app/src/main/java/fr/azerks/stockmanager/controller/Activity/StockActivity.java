package fr.azerks.stockmanager.controller.Activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import fr.azerks.stockmanager.R;
import fr.azerks.stockmanager.controller.Adapter.ItemsListAdapter;
import fr.azerks.stockmanager.controller.BDD.ItemDataBase;
import fr.azerks.stockmanager.controller.object.Item;

public class StockActivity extends AppCompatActivity  {

    private Button mAddBtn;
    private Button mRemoveBtn;
    private EditText mProductNameText;
    private EditText mProcuctQuantityText;
    private ListView mListViewProduct;
    private List<Item> mProductList = new ArrayList<>();
    public AlertDialog.Builder dialog;
    private SearchView mSearchView;
    private Boolean isRemove = false;
    private List<Item> removeCount = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock);

        mAddBtn = findViewById(R.id.m_add_btn);
        mRemoveBtn = findViewById(R.id.m_remove_btn);
        mListViewProduct = findViewById(R.id.m_list_view_product);
        mSearchView = findViewById(R.id.m_search_view);

        final ItemDataBase db = new ItemDataBase(this);
        db.open();
        mProductList.clear();
        mProductList.addAll(db.selectAll());
        db.close();

        final ItemsListAdapter adapter = new ItemsListAdapter(this, R.layout.edit_product_list, mProductList, isRemove);

        mListViewProduct.setAdapter(adapter);

        mAddBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ItemsListAdapter.isANewItemList = true;
                dialog.show();
            }
        });

        mRemoveBtn.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                if (isRemove){
                    if (removeCount != null){
                        for (int i = 0; i <removeCount.size(); i++) {
                            db.open();
                            db.remove(removeCount.get(i).getID());
                            mProductList.clear();
                            mProductList.addAll(db.selectAll());
                            db.close();
                        }
                    }
                    isRemove = false;
                    mListViewProduct.setAdapter(new ItemsListAdapter(StockActivity.this, R.layout.edit_product_list, mProductList, isRemove));
                }else {
                    isRemove = true;
                    mListViewProduct.setAdapter(new ItemsListAdapter(StockActivity.this, R.layout.edit_product_list, mProductList, isRemove));
                }
            }
        });

        mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                adapter.getFilter().filter("");
                mListViewProduct.clearTextFilter();
                return false;
            }
        });
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(TextUtils.isEmpty(newText)){
                    adapter.getFilter().filter("");
                    mListViewProduct.clearTextFilter();
                }
                else {
                    adapter.getFilter().filter(newText);
                }
                return true;
            }
        });

        mSearchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus){
                    mSearchView.setQuery("", false);
                }
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(StockActivity.this);
        this.dialog = builder;
        dialog.setPositiveButton("Confirm", new Dialog.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Dialog dialogObj = Dialog.class.cast(dialog);
                mProductNameText = dialogObj.findViewById(R.id.m_product_name_add_dialog);
                mProcuctQuantityText = dialogObj.findViewById(R.id.m_product_quantity_add_dialog);
                String productName = mProductNameText.getText().toString();
                String productQuantity = mProcuctQuantityText.getText().toString();

                if (productName.length() > 0 && mProcuctQuantityText.getText().length() > 0){
                    db.open();
                    db.insert(new Item(0, productName, Integer.valueOf(productQuantity), Item.Type.DEFAULT));
                    mProductList.clear();
                    mProductList.addAll(db.selectAll());
                    db.close();

                }
                else {
                    Toast t = new Toast(StockActivity.this).makeText(StockActivity.this, "Failed to add your product, please enter a valid name & quantity", Toast.LENGTH_LONG);
                    t.show();
                }
            }
        })
               .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                }).setTitle("Add Product").setView(R.layout.edit_product_list).create();

    }

    public void checkBoxHandler(View view){
        CheckBox cb = (CheckBox) view;
        int position = Integer.parseInt(cb.getTag().toString());
        View o = mListViewProduct.getChildAt(position).findViewById(R.id.m_selected_remove_checkbox);

        if (cb.isChecked()){
            removeCount.add(mProductList.get(position));
        }else if(!cb.isChecked()){
            removeCount.remove(mProductList.get(position));
        }
    }


    @SuppressLint("ResourceType")
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.layout.layout_menu, menu);
        return true;
    }
}
