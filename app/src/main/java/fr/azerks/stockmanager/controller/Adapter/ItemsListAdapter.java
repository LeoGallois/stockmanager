package fr.azerks.stockmanager.controller.Adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import fr.azerks.stockmanager.R;
import fr.azerks.stockmanager.controller.object.Item;

public class ItemsListAdapter extends ArrayAdapter<Item> implements Filterable {

    public Context mContext;
    private Boolean isRemoveLayout;
    List<Item> mItems, orig;
    public static boolean isANewItemList = true;
    static ViewHolder holder = new ViewHolder();

    public ItemsListAdapter(Context context, int resource, List<Item> item, Boolean isRemoveLayout) {
        super(context, resource, item);
        // TODO Auto-generated constructor stub
        mItems = item;
        this.orig = new ArrayList<Item>();
        this.mContext = context;
        this.isRemoveLayout = isRemoveLayout;
    }

    @Override
    public Item getItem(int position) {
        // TODO Auto-generated method stub
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
     /*   if(!isCopy){
            for (int i = 0; i < mItems.size(); i++) {
                orig.add(mItems.get(i));
            }
            isCopy = true;
    }*/
        //if (!orig.containsAll(mItems)) Collections.copy(mItems, orig);
        if (convertView == null) {
            LayoutInflater viewInflater;
            viewInflater = LayoutInflater.from(getContext());

            if(isRemoveLayout){
                convertView = viewInflater.inflate(R.layout.list_item_custom_stock_remove, null);
                holder.mRemoveCheckBox = convertView.findViewById(R.id.m_selected_remove_checkbox);
                holder.mRemoveCheckBox.setTag(position);
            }else {
                convertView = viewInflater.inflate(R.layout.list_item_custom_stock, null);
            }


            holder.mName = convertView.findViewById(R.id.m_product_name);
            holder.mQuantity = convertView.findViewById(R.id.m_quantity_text);
            holder.mAddQuantity = convertView.findViewById(R.id.m_add_quantity_btn);
            holder.mRemoveQuantity = convertView.findViewById(R.id.m_remove_quantity_btn);

            holder.mAddQuantity.setOnClickListener(holder.mOnClickListenerADD);
            holder.mRemoveQuantity.setOnClickListener(holder.mOnClickListenerREMOVE);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        //Log.i("Nomad", mItems.get(position).getItemName());
        Item item = getItem(position);
        if (item != null && !isRemoveLayout) {
        holder.mName.setText(item.getName());
        holder.mQuantity.setText(String.valueOf(item.getQuantity()));
        }else {
            holder.mName.setText(item.getName());
        }

        return convertView;
    }

    static class ViewHolder {
        public TextView mName;

        public TextView mQuantity;

        public Button mAddQuantity;

        public Button mRemoveQuantity;

        public CheckBox mRemoveCheckBox;

        View.OnClickListener mOnClickListenerADD = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int value = Integer.parseInt(String.valueOf(mQuantity.getText())) + 1;
                mQuantity.setText(String.valueOf(value));
            }
        };

        View.OnClickListener mOnClickListenerREMOVE = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int value = Integer.parseInt(String.valueOf(mQuantity.getText()));
                if (value > 0) {
                    value -= 1;
                    mQuantity.setText(String.valueOf(value));
                }
            }
        };
    }

    @Override
    public Filter getFilter() {
        if(isANewItemList) {
            for (int i = 0; i < mItems.size(); i++) {
                orig.add(mItems.get(i));
            }
        }
        isANewItemList = false;
        return new ItemFilter();
    }

    private class ItemFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            constraint = constraint.toString().toLowerCase();
            FilterResults result = new FilterResults();

            if (constraint != null && constraint.toString().length() > 0) {

                List<Item> founded = new ArrayList<Item>();
                for (Item item : mItems) {
                    if (item.getName().toLowerCase().contains(constraint)) {
                        founded.add(item);
                    }
                }
                result.values = founded;
                result.count = founded.size();
            } else {
                ArrayList<Item> list = new ArrayList<Item>(orig);
                result.values = list;
                result.count = list.size();
            }
            return result;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            clear();
            for (Item item : (List<Item>) results.values) {
                add(item);
            }
            notifyDataSetChanged();
        }

    }

}
