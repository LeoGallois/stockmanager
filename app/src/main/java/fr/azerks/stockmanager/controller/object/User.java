package fr.azerks.stockmanager.controller.object;

public class User {

    protected long ID;
    protected String username;
    protected String email;
    protected String password;

    public User(long ID, String username, String email, String password) {
        this.ID = ID;
        this.username = username;
        this.email = email;
        this.password = password;
    }

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
