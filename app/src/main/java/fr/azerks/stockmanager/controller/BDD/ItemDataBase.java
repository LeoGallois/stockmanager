package fr.azerks.stockmanager.controller.BDD;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.LinkedList;

import fr.azerks.stockmanager.controller.object.Item;

public class ItemDataBase {

    public static final int  DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "item.db";
    public static final String ITEM_TABLE_NAME = "stock";

    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String QUANTITY = "quantity";
    public static final String TYPE = "type";

    private SQLiteDatabase bdd;
    private  ItemOpenHelper itemSQLite;

    public ItemDataBase(Context context) {
        itemSQLite = new ItemOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void open(){
        bdd = itemSQLite.getWritableDatabase();
    }

    public void close(){
        bdd.close();
    }

    public SQLiteDatabase getBdd(){
        return bdd;
    }

    public long insert(Item m){
        ContentValues values = new ContentValues();
        values.put(NAME, m.getName());
        values.put(QUANTITY, m.getQuantity());
        values.put(TYPE, m.getType().toString());
        return bdd.insert(ITEM_TABLE_NAME,null, values);
    }

    public int update(int id, Item m){
        ContentValues values = new ContentValues();
        values.put(ID, m.getID());
        values.put(NAME, m.getName());
        values.put(QUANTITY, m.getQuantity());
        values.put(TYPE, m.getType().toString());
        return bdd.update(ITEM_TABLE_NAME, values, ID + " = " + id, null);
    }

    public int remove(long id){
        return  bdd.delete(ITEM_TABLE_NAME, ID + " = " + id, null);
    }

    public Item selectWithID(String[] args) {
        Cursor cursor = bdd.rawQuery("SELECT * from " + ITEM_TABLE_NAME + " WHERE id = ? ", args);
        Item item = cursorToItem(cursor, false);
        cursor.close();
        return item;
    }

    public LinkedList<Item> selectAll(){
        LinkedList<Item> articles = new LinkedList<Item>();
        Cursor cursor = bdd.rawQuery("SELECT * from " + ITEM_TABLE_NAME, null);
        if(cursor.getCount() != 0){
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                Item article = cursorToItem(cursor, true);
                articles.add(article);
            }
        }
        cursor.close();
        return articles;
    }

    public void displayArticles() {
        LinkedList<Item> articles = selectAll();
        for (int i = 0; i < articles.size(); i++) {
            System.out.println(articles.get(i));
        }
    }

    private Item cursorToItem(Cursor c, boolean multipleResult){
        if(!multipleResult) {
            c.moveToFirst();
        }
        Item item = new Item();
        item.setID(c.getInt(0));
        item.setName(c.getString(1));
        item.setQuantity(c.getInt(2));
        item.setType(Item.Type.valueOf(c.getString(3)));
        return item;
    }

}
